<?php
include_once ('simple_html_dom.php');
include_once( ABSPATH.'wp-admin/includes/plugin.php' );


// get html dom from file using CURL
// $maxlen is defined in the code as PHP_STREAM_COPY_ALL which is defined as -1.
function alma_file_get_html($url, $lowercase = true, $forceTagsClosed=true, $target_charset = DEFAULT_TARGET_CHARSET, $stripRN=true, $defaultBRText=DEFAULT_BR_TEXT, $defaultSpanText=DEFAULT_SPAN_TEXT)
{
	// We DO force the tags to be terminated.
	$dom = new simple_html_dom(null, $lowercase, $forceTagsClosed, $target_charset, $stripRN, $defaultBRText, $defaultSpanText);
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	$contents =  curl_exec($ch);
	curl_close ($ch);
	
	if (empty($contents) || strlen($contents) > MAX_FILE_SIZE)
	{
		return false;
	}
	// The second parameter can force the selectors to all be lowercase.
	$dom->load($contents, $lowercase, $stripRN);
	return $dom;
}


function PluginIsInstalled($metaContent, $all_plugins, &$pluginName)
{
	$pluginIsInstalled = false;
	$pluginName = $metaContent;
	
	//check if is installed
	foreach($all_plugins as $plug)
	{
		$strpos = strpos(strtolower(trim($metaContent)), strtolower(trim($plug['Name'])));
		
		if($strpos !== false) {
			$pluginIsInstalled  = true;
			
			//thi si necessary because sometimes the $plug['Name'] is a part of the $metaContent
			$pluginName = $plug['Name'];
			break;
		}
	}
	
	return $pluginIsInstalled;
}


function GetItemsPluginRepository($repository_list_file, $checkbox_name)
{
	$fileRepositoryList = fopen($repository_list_file, "r") or die("Unable to open file!");
	$all_plugins = get_plugins();
	$arrItems = array();
	$arrAction = array();
	$arrReturn = array();
										 
	while (($line1 = fgets($fileRepositoryList)) !== false) {
		$line1 = trim($line1); 
		
		if($line1 != '')
		{
			$urlWpPagePlugin = "https://wordpress.org/plugins/".strtolower(trim($line1))."/";
			$namePhpFile = "";
			$pluginName = "";
			 
			//parsing the page to get the url download
			$html = alma_file_get_html($urlWpPagePlugin);
			 
			if($html != null)
			{
				//get the plugin name
				foreach($html->find('meta') as $meta){
					if($meta->property == "og:title")
					{
						$metaContent = $meta->content;
						break;
					}
				}
				
			 	
				//get the url download plugin
				foreach($html->find('p.button a') as $a){
					$fileZip = $a->href;
					$pluginIsInstalled  = false;
					$pluginIsActive = false;
					 
					//check if is installed
					$pluginIsInstalled = PluginIsInstalled($metaContent, $all_plugins, $pluginName);
					 
					if($pluginIsInstalled)
					{
						//check if is active: search plugin data in every php file of this plugin
						$directoryPlugin = strtolower(get_home_path()."wp-content/plugins/".$line1."/");
						$arrFilesPlugin = scandir($directoryPlugin);
						$dataFilePluginFind = false;
						
						foreach($arrFilesPlugin as $nameFilePlugin)
						{
							if($nameFilePlugin != "." && $nameFilePlugin  != "..")
							{
								$arrTmp = explode('.', $nameFilePlugin);
								$file_extension = end($arrTmp);
								
								if(strtolower(trim($file_extension)) == "php")
								{
									 //read this file php
									 $filePhp = fopen($directoryPlugin.$nameFilePlugin, "r") or die("Unable to open file!");
									 
									 while (($line2 = fgets($filePhp)) !== false) {
										 $strposPluginName = strpos(strtolower(trim($line2)), "plugin name");
										 $strposName = strpos(strtolower(trim($line2)), strtolower($pluginName));

										 if($strposName > 0 && $strposPluginName !== false) 
										 {
											 //now I have the file data plugin
											 $namePhpFile = trim($line1."/".$nameFilePlugin);
											 
											 if (is_plugin_active($namePhpFile)) {
												$pluginIsActive = true;
											 } 

											 $dataFilePluginFind = true;
											 break;
										 }
									 }
									 
									 fclose($filePhp);
								}
							}
							
							if($dataFilePluginFind) break;
						}
					}

					if($pluginIsInstalled && $pluginIsActive)
					{
						array_push($arrItems, "<b>$pluginName</b> installato ed attivo");
						$arrAction[$fileZip] = "N|".$namePhpFile;
					}
					else if($pluginIsInstalled && !$pluginIsActive)
					{
						array_push($arrItems, "<input type='checkbox' name='".$checkbox_name."[]' id='$pluginName' value='".$fileZip."' /><b>$pluginName</b> : attiva il plugin");
						$arrAction[$fileZip] = "A|".$namePhpFile;
					}
					else
					{
						array_push($arrItems, "<input type='checkbox' name='".$checkbox_name."[]' id='$pluginName' value='".$fileZip."' /><b>$pluginName</b> : installa ed attiva il plugin");
						$arrAction[$fileZip] = "I| ";
					}
				}
			}
		}
	}
	 
	fclose($fileRepositoryList);
	array_push($arrReturn, $arrItems);
	array_push($arrReturn, $arrAction);
	return $arrReturn;
}


function GetItemsPluginAlmaware($plugin_zip_path, $checkbox_name)
{
	$arr_plugin_zip_list = scandir ($plugin_zip_path);
	$all_plugins = get_plugins();
	$arrItems = array();
	$arrAction = array();
	$arrReturn = array();
	
	foreach($arr_plugin_zip_list as $this_file)	{
		if($this_file != "." && $this_file  != "..")
		{
			 $zip = zip_open($plugin_zip_path.$this_file);
			 $dataFilePluginFind = false;
			 $pluginIsInstalled  = false;
			 $pluginIsActive = false;
			 $pluginName = "";
			 
			 if ($zip) {
				while ($zip_entry = zip_read($zip)){
					//read every file into file zip
					$namePhpFile = zip_entry_name($zip_entry);
					$arrTmp = explode('.', $namePhpFile);
					$file_extension = end($arrTmp);
					
					if(strtolower(trim($file_extension)) == "php")
					{
						if (zip_entry_open($zip, $zip_entry)) {
						   while (($line = zip_entry_read($zip_entry)) !== false) {					  
							  $strposPluginName = strpos(strtolower(trim($line)), "plugin name");
							  
							  if($strposPluginName < 1) break; //this is not file with plugin data
							  
							  $strposVersion = strpos(strtolower(trim($line)), "version", $strposPluginName);
							  $strposDescription = strpos(strtolower(trim($line)), "description", $strposPluginName);
							  $strposPluginURI = strpos(strtolower(trim($line)), "plugin uri", $strposPluginName);
							  $strposAuthor = strpos(strtolower(trim($line)), "author", $strposPluginName);
							  $strposAuthorURI = strpos(strtolower(trim($line)), "author uri", $strposPluginName);
							  $strlenPlugiName = strlen("plugin name");
							  $strposX = min($strposVersion, $strposDescription, $strposPluginURI, $strposAuthor, $strposAuthorURI);
							  
							  $strlenPluginName = $strposX - $strposPluginName - $strlenPlugiName;
							   
							  if($strposX > 0 && $strposPluginName !== false) 
							  {
								  //file php with plugin data found!
								  $pluginName = substr(strtolower(trim($line)), $strposPluginName + $strlenPlugiName, $strlenPluginName);
								  $pluginName = trim(str_replace(":", "", $pluginName));
								  $dataFilePluginFind = true;
								  break;
							  }
						  }
						  
						  zip_entry_close($zip_entry);
						}
					}
					
					if($dataFilePluginFind) break;
				}
				zip_close($zip);
				
				if($dataFilePluginFind)
				{
					//check if is installed
					$pluginIsInstalled = PluginIsInstalled($pluginName, $all_plugins, $pluginName);

					//check if is active
					if ($pluginIsInstalled) {
						if (is_plugin_active($namePhpFile) ) {
							$pluginIsActive = true;
						} 
					}
				}
			}
			
			$tmpName = $pluginName;
			if($tmpName == "") $tmpName = $this_file;
				
			if($pluginIsInstalled && $pluginIsActive)
			{
				array_push($arrItems, "<b>$tmpName</b> installato ed attivo");
				$arrAction[$this_file] = "N|".$namePhpFile;
			}
			else if($pluginIsInstalled && !$pluginIsActive)
			{
				array_push($arrItems, "<input type='checkbox' name='".$checkbox_name."[]' id='$tmpName' value='$this_file'  /><b>$tmpName</b> : attiva il plugin");
				$arrAction[$this_file] = "A|".$namePhpFile;
			}
			else
			{
				array_push($arrItems, "<input type='checkbox' name='".$checkbox_name."[]' id='$tmpName' value='$this_file'  /><b>$tmpName</b> : installa ed attiva il plugin");
				$arrAction[$this_file] = "I| ";
			}
		} 
	} 
	
	array_push($arrReturn, $arrItems);
	array_push($arrReturn, $arrAction);
	return $arrReturn;
}


?>