<?php
include('simple_html_dom.php');
include('alma-functions.php');
include_once( ABSPATH.'wp-admin/includes/plugin.php' );

$mpiObj = new almainstaller();
?>
<div class="wrap pc-wrap">
	<div class="mpiicon icon32"></div>
	<h2><?php _e('Almaware Toolcase '.alma_mpi_get_version().'','mpi') ?></h2>
	<?php
		if (!current_user_can('edit_plugins')) { 
			_e('You do not have sufficient permissions to manage plugins on this blog.<br>','mpi');
			return;
		}
	?>
	<div id="mpiblock">

			
		<div style="text-align:right;"><a href="javascript:void(0);" id="mpi-expand"><?php _e('Expand All','mpi') ?></a>&nbsp;<a href="javascript:void(0);" id="mpi-collapse"><?php _e('Collapse All','mpi') ?></a></div>
		
		<div><?php if($mpiObj->mpi_app_DirTesting()){} else{ _e('<div class="mpi_error">oops!!! Seems like the directory permission are not set right so some functionalities of plugin will not work.<br/>Please set the directory permission for the folder "uploads" inside "wp-content" directory to 777.</div>','mpi'); } ?></div>
		
		
		<br />	
		<div id="poststuff" class="mpi-meta-box">
			<div class="postbox">
				<h3 class="hndle"><span><?php _e('ATTENZIONE! La cartella del plugin deve essere nominata almaware-toolcase'); ?></span></h3>
			</div>		
		</div>
			
		 <!-- Install Plugins usually used from repository    -->
			<div id="poststuff" class="mpi-meta-box">
				<div class="postbox">
					<div class="handlediv" title="Click to toggle"><br/></div>
					<h3 class="hndle"><span><?php _e('Plugin comunemente utilizzati'); ?></span></h3>
					<div class="inside">
						<form name="form_apu" method="post" action="">
							 <?php wp_nonce_field($mpiObj->key); ?>
							 <?php
									//Read from repository_list.txt file
									$repository_list_file = plugin_dir_path(__FILE__)."repository_list.txt" ;
									$all_plugins = get_plugins();
									$checkbox_name = 'cbPluginRepository';
									$arrAction = null;
									
									if(file_exists($repository_list_file))
									{
										$arrReturn = GetItemsPluginRepository($repository_list_file, $checkbox_name);
									    $arrItems = $arrReturn[0];
										$arrAction = $arrReturn[1];
										
										foreach($arrItems as $item)
										{
											echo($item."<br />");
										}
									}
									else{
										echo("<div class='mpi_error' >Impossibile caricare la lista dei plugin!!!. File repository_list.txt non trovato</div>");
									}
							?>
							<br/><br/>
							<div>
								<input style="float:left; width: 350px;"  class="button button-primary mpi_button" type="submit" name="mpi_wpActivate" value="<?php _e('Installa/Attiva i plugins selezionati','mpi'); ?>" />
								<div class="mpi_clear"></div>
							</div>
						</form>
						<?php
							if(isset($_POST['mpi_wpActivate']) && isset($_POST['cbPluginRepository']) && is_array($_POST['cbPluginRepository']))
							{
								$mpiObj->alma_app_wpRepositoryInstall('activate', $arrAction, $checkbox_name);
							}	
						?>
					</div>
				</div>		
			</div>
		 <!-- Install Plugins usually used from repository    -->
		
	
	   <!-- Install Plugins Almaware from directory plugin-zip  -->
		<div id="poststuff" class="mpi-meta-box">
			<div class="postbox">
				<div class="handlediv" title="Click to toggle"><br/></div>
				<h3 class="hndle"><span><?php _e('Plugin Almaware'); ?></span></h3>
				<div class="inside">
					<form name="form_apu" method="post" action="">
						 <?php wp_nonce_field($mpiObj->key); ?>
						 <?php
								//Search into plugin-zip directory
								$plugin_zip_path = plugin_dir_path(__FILE__)."plugin-zip/" ;
								$checkbox_name = 'cbPluginZip';
								$arrAction = null;
								
								if(is_dir($plugin_zip_path))
								{
									$arrReturn = GetItemsPluginAlmaware($plugin_zip_path, $checkbox_name);
									$arrItems = $arrReturn[0];
									$arrAction = $arrReturn[1];
									
									foreach($arrItems as $item)
									{
										echo($item."<br />");
									}
								}
								else{
									echo("<div class='mpi_error' >Impossibile caricare la lista dei plugin Almaware!!!. Cartella plugin-zip non trovata</div>");
								}
						?>
						<br/><br/>
						<div>
							<input style="float:left; width: 350px;"  class="button button-primary mpi_button" type="submit" name="mpi_wpActivate" value="<?php _e('Installa/Attiva i plugins selezionati','mpi'); ?>" />
							<div class="mpi_clear"></div>
						</div>
					</form>
					<?php
						if(isset($_POST['mpi_wpActivate'])  && isset($_POST[$checkbox_name]) && is_array($_POST[$checkbox_name]))
						{
							$mpiObj->alma_app_wpZipInstall('activate', $arrAction, $checkbox_name);
						}	
					?>
				</div>
			</div>		
		</div>
	    <!-- Install Plugins Almaware from directory plugin-zip  -->
	   
	</div>
</div>